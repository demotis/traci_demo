// Wait for PhoneGap to load
document.addEventListener("deviceready", onDeviceReady, false);

// PhoneGap is ready
function onDeviceReady()
{
    $("#workPat_LV").kendoMobileListView({
        dataSource: workingPatList,
        template: "<a class='reply' data-role='button' data-rel='actionsheet' href='\\#patSelectActions' data-actionsheet-context='#:ID#'>Action</a> <h2>#: PatFullName#</h2>",
        endlessScroll: true
    });

    $("#fullPat_LV").kendoMobileListView({
        dataSource: fullPatList,
        template: "<input type='checkbox' data-role='switch' data-on-label='ADD' data-off-label='' class='fpl_cb' name='#:ID#' id='#:ID#'><h2>#: PatFullName#</h2>",
        endlessScroll: true
    });

    $("#selPat_LV").kendoMobileListView({
        dataSource: selPatList,
        template: "<a class='reply' data-role='button' data-rel='actionsheet' href='\\#patCDSTree_AS' data-actionsheet-context='#:id#'>Action</a> <h2>#: name#</h2>",
        endlessScroll: false
    });

    $("#CDS_Trees_LV").kendoMobileListView({
        dataSource: CDSTreesList,
        template: "<a class='reply' data-role='button' data-rel='actionsheet' href='\\#startCDSTree_AS' data-actionsheet-context='#:id#'>Action</a> <h2>#: name#</h2>",
        endlessScroll: false
    });
    
    $("#answers_LV").kendoMobileListView({
        dataSource: AnswersList,
        template: "<a class='reply' data-role='button' data-rel='actionsheet' data-click='answerQuestion' data-answerindex='#:A_Index#'>Select</a> <h2>#: A_Text#</h2>",
        endlessScroll: false
    });
}

function saveNewPat()
{
    var checkedBoxes = $('.fpl_cb:checked');
    
    if (checkedBoxes.length == 0)
        app.navigate('#tabstrip-patient_list');
    
    $.ajax({
        url: 'http://' + dataServer + dataPath + 'set_working_patient.ajax.php',
        type: "post",
        data: checkedBoxes.serialize(),
        dataType: 'json',
        success: function (data)
        {
            app.navigate('#tabstrip-patient_list');
        }
    });
}

function patient_list_show()
{
    wpl_patSearch();
}

function add_patient_show()
{
    fpl_patSearch();
}


//
// SECTION FOR WORKING PATIENT LIST AND SEARCH OPTIONS
//

function wpl_patSearch()
{
    wpl_searchTerms = $("#wpl_searchWords").val();
    wpl_newSearch = 1;
    var lvSearch = $("#workPat_LV").data("kendoMobileListView");
    
    if (lvSearch == null)
        return;
    
    lvSearch.refresh();
    lvSearch.dataSource.read();
    $('.km-content:visible').data('kendoMobileScroller').reset();
}

var wpl_searchTerms = "";
var wpl_newSearch = 0;
var workingPatList = new kendo.data.DataSource({
    pageSize: 30,
    serverPaging: true, //specifies whether the paging should be handled by the service
    change: function() {
        //if (!this.view()[9])
        //    $("#patListView").data("kendoMobileListView").stopEndlessScrolling();
    },
    transport: {
        read: {
            url: 'http://' + dataServer + dataPath + 'wpl.ajax.php', // the remote service url
            dataType: "json" // JSONP (JSON with padding) is required for cross-domain AJAX
        },
        parameterMap: function(options) {
            var parameters = {
                q: wpl_searchTerms, //additional parameters sent to the remote service
                rpp: options.pageSize,
                page: wpl_newSearch //next page
            };
            wpl_newSearch = 0;
            return parameters;
        }
    },
    schema: { // describe the result format
        data: "results" // the data which the DataSource will be bound to is in the "results" field
    }
});

//
// END SECTION FOR WORKING PATIENT LIST AND SEARCH OPTIONS
//


//
// SECTION FOR FULL PATIENT LIST AND SEARCH OPTIONS
//

function fpl_patSearch()
{
    fpl_searchTerms = $("#fpl_searchWords").val();
    fpl_newSearch = 1;
    var lvSearch = $("#fullPat_LV").data("kendoMobileListView");
    lvSearch.refresh();
    lvSearch.dataSource.read();
    $('.km-content:visible').data('kendoMobileScroller').reset();
}

var fpl_searchTerms = "";
var fpl_newSearch = 0;
var fullPatList = new kendo.data.DataSource({
    pageSize: 30,
    serverPaging: true, //specifies whether the paging should be handled by the service
    change: function() {
        //if (!this.view()[9])
        //    $("#patListView").data("kendoMobileListView").stopEndlessScrolling();
    },
    transport: {
        read: {
            url: 'http://' + dataServer + dataPath + 'fpl.ajax.php', // the remote service url
            dataType: "json" // JSONP (JSON with padding) is required for cross-domain AJAX
        },
        parameterMap: function(options) {
            var parameters = {
                q: fpl_searchTerms, //additional parameters sent to the remote service
                rpp: options.pageSize,
                page: fpl_newSearch //next page
            };
            fpl_newSearch = 0;
            return parameters;
        }
    },
    schema: { // describe the result format
        data: "results" // the data which the DataSource will be bound to is in the "results" field
    }
});

//
// END SECTION FOR FULL PATIENT LIST AND SEARCH OPTIONS
//

function onOpenCDSTree_AS(e)
{
    this.element.find(".km-actionsheet-title").text(e.target.next().text());
}

function onOpen(e) {
    this.element.find(".km-actionsheet-title").text(e.target.next().text());
}

function openPat(e) {
    activePatient = e.context;
    activeCDSTree = 0;
    app.navigate('#tabstrip-patient');
}

function removePat(e) {
        
}

var activePatient = 0;
function onOpenPatent()
{
    $.ajax({
        url: 'http://' + dataServer + dataPath + 'get_patient.ajax.php',
        type: "post",
        data: { 'id': activePatient},
        dataType: 'json',
        success: function (data)
        {
            $("#patientNavView").data("kendoMobileNavBar").title(data[0].PatName);
            $("#selPat_Name").html(data[0].PatName);
            $("#selPat_ID").html(data[0].ID);
            
            var lvSearch = $("#selPat_LV").data("kendoMobileListView");
            lvSearch.refresh();
            lvSearch.dataSource.read();
            $('.km-content:visible').data('kendoMobileScroller').reset();
        }
    });

}

var selPatList = new kendo.data.DataSource({
    pageSize: 100,
    serverPaging: false, //specifies whether the paging should be handled by the service
    change: function()
    {
        
    },
    transport:
    {
        read:
        {
            url: 'http://' + dataServer + dataPath + 'get_patient_cdstrees.ajax.php', // the remote service url
            dataType: "json" // JSONP (JSON with padding) is required for cross-domain AJAX
        },
        parameterMap: function(options)
        {
            var parameters = {
                q: activePatient
            };
            return parameters;
        }
    },
    schema:
    {
        data: "results"
    }
});


function startCDSTree()
{
    if (activePatient == 0)
        return;
    app.navigate('#tabstrip-add_CDSTree');
}

function add_CDSTree_show()
{
    
}

var activeCDSTree = 0;
function openCDS(e) {
    activeCDSTree = e.context;
    app.navigate('#tabstrip-cds_tree');
    
    return;
    $.ajax({
        url: 'http://' + dataServer + dataPath + 'get_patient.ajax.php',
        type: "post",
        data: { 'id': e.context},
        dataType: 'json',
        success: function (data)
        {
            app.navigate('#tabstrip-patient');
            $("#patientNavView").data("kendoMobileNavBar").title(data[0].name_last + ', ' + data[0].name_first);
        }
    });
}




function onOpenStartCDSTree_AS(e)
{
    this.element.find(".km-actionsheet-title").text(e.target.next().text());
}

function startCDS(e) {
    $.ajax({
        url: 'http://' + dataServer + dataPath + 'set_cdstree.ajax.php',
        type: "post",
        data: { 'tree_id': e.context, 'pat_id' : activePatient },
        dataType: 'json',
        success: function (data)
        {
            app.navigate('#tabstrip-patient');
        }
    });
}


var CDSTreesList = new kendo.data.DataSource({
    pageSize: 100,
    serverPaging: false, //specifies whether the paging should be handled by the service
    change: function()
    {
        
    },
    transport:
    {
        read:
        {
            url: 'http://' + dataServer + dataPath + 'get_cdstrees.ajax.php', // the remote service url
            dataType: "json" // JSONP (JSON with padding) is required for cross-domain AJAX
        },
        parameterMap: function(options)
        {
            var parameters = {
                q: ''
            };
            return parameters;
        }
    },
    schema:
    {
        data: "results"
    }
});




















function onOpenCDSTree()
{
    $.ajax({
        url: 'http://' + dataServer + dataPath + 'get_cdstree.ajax.php',
        type: "post",
        data: { 'pat_id': activePatient, 'tree_id' : activeCDSTree},
        dataType: 'json',
        success: function (data)
        {
            $("#cds_treeNavView").data("kendoMobileNavBar").title(data[0].patName);
            $("#CDSTree_Name").html(data[0].treeName);
            activeQuestionID = data[0].Q_ID;
            $("#question").html(data[0].Q_Text);
            activeAnswerID = data[0].A_ID;
            
            var lvSearch = $("#answers_LV").data("kendoMobileListView");
            lvSearch.refresh();
            lvSearch.dataSource.read();
            $('.km-content:visible').data('kendoMobileScroller').reset();
        }
    });
}

var activeAnswerID = 0;
function answerQuestion(e)
{
    var data = e.button.data();
    $.ajax({
        url: 'http://' + dataServer + dataPath + 'set_cds_answer.ajax.php',
        type: "post",
        data: { 'a_index': data.answerindex, 'a_id' : activeAnswerID, 'q_id': activeQuestionID, 'p_id': activePatient, 't_id' : activeCDSTree},
        dataType: 'json',
        success: function (data)
        {
            onOpenCDSTree();
        }
    });
}

var activeQuestionID = 0;

var AnswersList = new kendo.data.DataSource({
    pageSize: 100,
    serverPaging: false, //specifies whether the paging should be handled by the service
    change: function()
    {
        
    },
    transport:
    {
        read:
        {
            url: 'http://' + dataServer + dataPath + 'get_cds_answers.ajax.php', // the remote service url
            dataType: "json" // JSONP (JSON with padding) is required for cross-domain AJAX
        },
        parameterMap: function(options)
        {
            var parameters = {
                q: activeQuestionID
            };
            return parameters;
        }
    },
    schema:
    {
        data: "results"
    }
});