// JavaScript Document
var loaderElement;

// Wait for PhoneGap to load
document.addEventListener("deviceready", onDeviceReady, false);

// PhoneGap is ready
function onDeviceReady()
{
    $("#modalview-login").kendoMobileModalView("open");
    loaderElement = app.pane.loader.element.find("h1");
}

function processLogin()
{
    $("#modalview-login").kendoMobileModalView("close");
    startLoader();
    
    var form = $("#login_form");
    var serializedData = form.serialize();
    
    $.ajax({
        url: 'http://' + dataServer + dataPath + 'login.ajax.php',
        type: "post",
        data: serializedData,
        dataType: 'json',
        success: function (data)
        {
            console.log(data);
            if (data.loginValid == '1')
            {
                window.location = "main_console.html";
            }
            else
            {
                hideLoader();
                $("#modalview-login").kendoMobileModalView("open");
            }
        }
    });
}

function startLoader()
{
    hideLoader();
    app.showLoading(); //show loading popup
}

function hideLoader()
{
    app.hideLoading(); //hide loading popup
    loaderElement.removeClass("loaderHeading").text("Loading...");
}

